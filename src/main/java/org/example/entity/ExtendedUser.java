package org.example.entity;

import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;
import org.apache.beam.sdk.schemas.annotations.SchemaCreate;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import org.apache.avro.reflect.Nullable;

@DefaultCoder(AvroCoder.class)
public class ExtendedUser implements Serializable {
    private String email;
    private int age;
    private String userName;

    @Nullable
    public String shortUserName;
    @Nullable
    private List<String> errors;

    @SchemaCreate
    public ExtendedUser(String email,
                        int age,
                        String userName,
                        @Nullable String shortUserName,
                        @Nullable List<String> errors
    ) {
        this.email = email;
        this.age = age;
        this.userName = userName;
        this.shortUserName = shortUserName;
        this.errors = errors;
    }

    public ExtendedUser() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Nullable
    public String getShortUserName() {
        return shortUserName;
    }

    public void setShortUserName(@Nullable String shortUserName) {
        this.shortUserName = shortUserName;
    }

    @Nullable
    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(@Nullable List<String> errors) {
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExtendedUser that = (ExtendedUser) o;
        return age == that.age &&
                Objects.equals(email, that.email) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(shortUserName, that.shortUserName) &&
                Objects.equals(errors, that.errors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, age, userName, shortUserName, errors);
    }

    @Override
    public String toString() {
        return "ExtendedUser{" +
                "email='" + email + '\'' +
                ", age=" + age +
                ", userName='" + userName + '\'' +
                ", shortUserName='" + shortUserName + '\'' +
                ", errors=" + errors +
                '}';
    }
}