package org.example.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;

import java.util.List;

@Data
@NoArgsConstructor
@DefaultCoder(AvroCoder.class)
public class InvalidUser  {
    private String email;
    private int age;
    private String userName;
    private List<String> errors;
}
