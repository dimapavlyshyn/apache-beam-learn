package org.example.entity;

import lombok.*;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;
import org.apache.beam.sdk.schemas.JavaBeanSchema;
import org.apache.beam.sdk.schemas.annotations.DefaultSchema;
import org.apache.beam.sdk.schemas.annotations.SchemaCreate;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgeString implements Serializable {
    public int age;
    public String ageString;
}
