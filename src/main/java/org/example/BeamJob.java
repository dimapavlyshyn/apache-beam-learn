package org.example;

import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.AvroIO;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.*;
import org.apache.commons.beanutils.BeanUtils;
import org.example.entity.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


public class BeamJob {
    public static void main(String[] args) {
        // Start by defining the options for the pipeline.
        val options = PipelineOptionsFactory.fromArgs(args)
                .withValidation()
                .as(BeamJobOptions.class);

        val p = Pipeline.create(options);
        p.getSchemaRegistry().registerJavaBean(User.class);
        p.getSchemaRegistry().registerJavaBean(ExtendedUser.class);

        val users = p.apply(AvroIO.read(User.class)
                .from(options.getUserSourcePath())
        );

        val ageString = p.apply(AvroIO.read(AgeString.class)
                .from(options.getAgeStringSourcePath())
        );

        val successTag = new TupleTag<ExtendedUser>() {
        };
        val errorTag = new TupleTag<ExtendedUser>() {
        };

        val successFailedUsers = users.apply(ParDo.of(new DoFn<User, ExtendedUser>() {

            @ProcessElement
            @SneakyThrows
            public void processElement(@Element User exUser, MultiOutputReceiver out) {
                val newExUser = new ExtendedUser();
                BeanUtils.copyProperties(newExUser, exUser);
                val errors = new ArrayList<String>();
                if (newExUser.getAge() < 20) {
                    errors.add("Age < 20");
                }
                if (!newExUser.getEmail().endsWith("@gmail.com")) {
                    errors.add("Email Does't have gmail domain");
                }
                if (errors.isEmpty()) {
                    newExUser.setShortUserName(newExUser.getEmail().replaceFirst("@.*$", ""));
                }
                newExUser.setErrors(errors);
                out.get(errors.isEmpty() ? successTag : errorTag)
                        .output(newExUser);

            }
        }).withOutputTags(successTag, TupleTagList.of(errorTag)));

        val errorUsers = successFailedUsers.get(errorTag);
        val successUsers = successFailedUsers.get(successTag);


        val ageStringTag = new TupleTag<AgeString>() {
        };
        val ageStringView = ageString.apply(View.asList());

        val result = successUsers.apply(ParDo.of(new DoFn<ExtendedUser, ValidUser>() {
            @SneakyThrows
            @ProcessElement
            public void processElement(@Element ExtendedUser user, OutputReceiver<ValidUser> out, ProcessContext c) {
                val newUser = new ValidUser();
                BeanUtils.copyProperties(newUser, user);
                val ageStringLookup = c.sideInput(ageStringView);
                val foundAgeString = ageStringLookup
                        .stream()
                        .filter((as) -> as.getAge() == newUser.getAge())
                        .findAny()
                        .map(AgeString::getAgeString)
                        .orElseGet(() -> "UNKNOWN");
                newUser.setAgeString(foundAgeString);
                out.output(newUser);
            }
        }).withSideInput(ageStringTag.getId(), ageStringView));

        result.apply(new Show<>());

        result.apply(ParDo.of(new ToJson<>()))
                .apply(TextIO.write().to(options.getTargetPath() + "/success/data")
                        .withSuffix(".json"));

        errorUsers
                .apply(MapElements.into(TypeDescriptor.of(InvalidUser.class)).via(s -> {
                    val invalidUser = new InvalidUser();
                    try {
                        BeanUtils.copyProperties(invalidUser, s);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    return invalidUser;
                }))
                .apply(ParDo.of(new ToJson<>()))
                .apply(TextIO.write().to(options.getTargetPath() + "/error/data")
                        .withSuffix(".json"));


        p.run().waitUntilFinish();
    }

    public static class ToJson<T> extends DoFn<T, String> {
        @ProcessElement
        public void processElement(@Element T obj, OutputReceiver<String> out) {
            val gs = new GsonBuilder().disableHtmlEscaping().create();

            out.output(gs.toJson(obj));
        }
    }


    public static class Show<T> extends PTransform<PCollection<T>, PCollection<Void>> {

        @Override
        public PCollection<Void> expand(PCollection<T> input) {


            val jsonObject = input.apply(ParDo.of(new ToJson<>()));
            return jsonObject.apply(MapElements.into(TypeDescriptors.voids()).via((el) -> {
                System.out.println(el);
                return null;
            }));

        }
    }


}
