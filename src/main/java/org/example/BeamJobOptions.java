package org.example;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.ValueProvider;

public interface BeamJobOptions extends PipelineOptions {
    @Description("User source path")
    @Default.String("D:\\Projects\\Education\\users.avro")
    ValueProvider<String> getUserSourcePath();

    void setUserSourcePath(ValueProvider<String> value);

    @Description("Age String source path")
    @Default.String("D:\\Projects\\Education\\ageString.avro")
    ValueProvider<String> getAgeStringSourcePath();

    void setAgeStringSourcePath(ValueProvider<String> value);

    @Description("Target path")
    @Default.String("./output")
    String getTargetPath();

    void setTargetPath(String value);




}